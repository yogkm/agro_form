from django.db import models


class Harvester(models.Model):

    load_num = models.IntegerField()
    harvester = models.CharField(max_length=255)
    date = models.DateField()
    time = models.TimeField()
    field = models.CharField(max_length=255)
    harvest_type = models.CharField(max_length=255)
    variety = models.CharField(max_length=255)
    truck = models.IntegerField()
    acres_harvested = models.DecimalField(max_digits=6, decimal_places=2)
    passes = models.DecimalField(max_digits=6, decimal_places=2)
    comments = models.TextField()
    cwt = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.harvester


class Farm(models.Model):

    load_num = models.ForeignKey('Harvester', on_delete=models.CASCADE)
    date = models.DateField()
    time = models.TimeField()
    tub_no = models.IntegerField()
    bin_no = models.IntegerField()
    b_trks = models.DecimalField(max_digits=6, decimal_places=2)
    pulp_temp = models.DecimalField(max_digits=6, decimal_places=2)
    comments = models.TextField()

    def __str__(self):
        return "{}, Tub# {}, Bin# {}".format(self.load_num, self.tub_no, self.bin_no)
