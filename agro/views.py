from django import forms
from django.shortcuts import reverse, render
from django.http import HttpResponseRedirect
from django.views import View, generic

from .forms import HarvesterForm, FarmForm
from .models import Harvester, Farm


class IndexView(generic.ListView):
    model = Harvester
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['harvesters'] = Harvester.objects.all()
        return context


class HarvesterFormView(View):
    form_class = HarvesterForm
    template_name = 'agro/harvester.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.GET)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            harvester = form.save()
            request.session['harvester_id'] = harvester.id
            return HttpResponseRedirect(reverse('agro:farm_view'))
        return render(request, self.template_name, {'form': form, 'errors': form.errors})


class FarmFormView(View):
    form_class = FarmForm
    template_name = 'agro/farm.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.GET)
        context = dict()
        if 'harvester_id' in request.session:
            context['harvester_id'] = request.session.pop('harvester_id')
            form.fields['load_num'].required = False
            form.fields['load_num'].widget = forms.HiddenInput()
        context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            farm = form.save()
            return HttpResponseRedirect(reverse('agro:index_view'))
        return render(request, self.template_name, {'form': form, 'errors': form.errors})
