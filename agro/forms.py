from django import forms

from .models import Harvester, Farm


class HarvesterForm(forms.ModelForm):

    class Meta:
        model = Harvester
        fields = ['load_num', 'harvester', 'date', 'time', 'field', 'harvest_type', 'variety', 'truck',
                  'acres_harvested', 'passes', 'comments', 'cwt']


class FarmForm(forms.ModelForm):

    class Meta:
        model = Farm
        fields = ['load_num', 'date', 'time', 'tub_no', 'bin_no', 'b_trks', 'pulp_temp', 'comments']
