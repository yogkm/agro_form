from django.urls import path

from .views import HarvesterFormView, FarmFormView, IndexView

app_name = 'agro'

urlpatterns = (
    path('', IndexView.as_view(), name='index_view'),
    path('harvester/', HarvesterFormView.as_view(), name='harvester_view'),
    path('farm/', FarmFormView.as_view(), name='farm_view'),
)
