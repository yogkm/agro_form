from django.contrib import admin

from .models import Harvester, Farm

admin.site.register(Harvester)
admin.site.register(Farm)
